package com.scotiabank.controller;

import com.scotiabank.model.Usuario;
import com.scotiabank.service.userService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class controllerlogin {

    @Autowired
    userService userService;

    @GetMapping("/login")
    public String login()
    {
        return "login";
    }

    @GetMapping("/error")
    public String error()
    {
        return "error";
    }

    @PostMapping({"/welcome"})
    public String welcome(Model model,
            @RequestParam(name="name", required=false) String name,
            @RequestParam(name="password",required = false) String password)
    {

        Usuario usuario = userService.getuser();
        Integer idcustomer = usuario.getIdcustomer();
        String apellido = usuario.getApellido();
        model.addAttribute("name", name);
        model.addAttribute("password", password);
        Integer valor = Integer.valueOf(password);

        if (!idcustomer.equals(valor) || !apellido.equals(name)){

            return "error";
        }
        else {

            return "welcome";
        }
    }
}
