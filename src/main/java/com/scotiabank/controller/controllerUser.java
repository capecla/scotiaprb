package com.scotiabank.controller;


import com.scotiabank.model.Usuario;
import com.scotiabank.service.userService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/service", produces = "application/json", method = {RequestMethod.DELETE, RequestMethod.POST, RequestMethod.PUT, RequestMethod.PATCH})
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.PATCH, RequestMethod.DELETE})
public class controllerUser {

    @Autowired
    userService userService;

    @GetMapping("/consulta/{idcustomer}")
    public List<Usuario> ConsultaPermisosRol(
            @PathVariable Integer idcustomer
    ) {
        return userService.getuserid(idcustomer);
    }
}

