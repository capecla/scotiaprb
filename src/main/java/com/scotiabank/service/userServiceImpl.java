package com.scotiabank.service;

import com.scotiabank.model.Usuario;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Service
public class userServiceImpl implements userService{

    @Override
    public Usuario getuser() {

        Usuario usuario = new Usuario();
        usuario.setIdcustomer(1);
        usuario.setNombre("pedro");
        usuario.setApellido("pedraza");
        usuario.setDireccion("calle 129");
        usuario.setEmail("popedraza");
        usuario.setTelefono("2452222");
        return usuario;
    }

    @Override
    public List<Usuario> getuserid(Integer idcustomer) {

        List list = new ArrayList();

        Usuario usuario = new Usuario();
        usuario.setIdcustomer(1);
        usuario.setNombre("pedro");
        usuario.setApellido("pedraza");
        usuario.setDireccion("calle 129");
        usuario.setEmail("popedraza");
        usuario.setTelefono("2452222");



        Integer valor = usuario.getIdcustomer();

        if (valor.equals(idcustomer)){
            list = Arrays.asList(usuario);
            return list;
        }
        else {

            list.add(0,"Error usuario no existe");
            return list;
        }
    }

}
